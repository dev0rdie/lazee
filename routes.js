const routes = require('next-routes')
                                                    
module.exports = routes()
.add('admin', '/admin', 'admin')
.add('index', '/', 'blog')
.add('blog', '/', 'blog')                        
.add('about', '/about', 'about')                                       
.add('article', '/article/:category/:slug')
.add('projects', '/projects', 'projects')
.add('sitemap', '/map', 'sitemap')
.add('error', '/_error', '_error');