import {Navbar, Alignment, Colors, AnchorButton} from '@blueprintjs/core';
import SocialLogo from 'social-logos';
import Link from 'next/link';

export default function Footer() {
    return (
        <Navbar style={{ color: Colors.WHITE, background: Colors.DARK_GRAY3, marginTop: '30px' }} align={Alignment.CENTER}>
            <Navbar.Group style={{ color: Colors.white, textAlign: 'center', width: '100%' }}>
                <p style={{width: '100%', margin: '0px'}}>All credits to lazee</p>
            </Navbar.Group>
        </Navbar>
    )
}