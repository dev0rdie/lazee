import {Navbar, Alignment, Colors, AnchorButton} from '@blueprintjs/core';
import SocialLogo from 'social-logos';
import Link from 'next/link';

export default function Header() {
    return (
        <Navbar style={{ color: Colors.WHITE, background: Colors.DARK_GRAY3 }}>
            <Navbar.Group align={Alignment.LEFT} style={{ color: Colors.white }}>
                <Navbar.Heading>Lazee</Navbar.Heading>
                <Navbar.Divider style={{ background: Colors.WHITE }}/>
                <AnchorButton text="About" style={{ color: Colors.WHITE, background: Colors.DARK_GRAY1, marginLeft: '10px'}}/>
                <AnchorButton text="Projects" style={{ color: Colors.WHITE, background: Colors.DARK_GRAY1, marginLeft: '10px' }}/>
                <AnchorButton text="Admin" style={{ color: Colors.WHITE, background: Colors.DARK_GRAY1, marginLeft: '10px' }}/>
            </Navbar.Group>
            <Navbar.Group style={{ color: Colors.white }}>
            <input class="bp3-input" type="search" placeholder="Search" dir="auto" style={{marginLeft: '100px', width: '500px', position: 'relative', left: '50%'}}/>
            </Navbar.Group>
            <Navbar.Group align={Alignment.RIGHT} style={{ color: Colors.white }}>
            <Link href="https://github.com/getriot">
                <a style={{ color: Colors.WHITE, background: Colors.DARK_GRAY3 }} target="_blank"><SocialLogo icon="github" size={ 28 }/></a>
            </Link>
            <Link href="https://twitch.tv/g3tri0t">
                <a style={{ color: Colors.WHITE, background: Colors.DARK_GRAY3 }} target="_blank"><SocialLogo icon="twitch" size={ 28 }/></a>
            </Link>
            </Navbar.Group>
        </Navbar>
    )
}