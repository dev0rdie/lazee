import Document, { Head, Main, NextScript } from 'next/document';
import Header from '../src/components/header/header';
import Footer from '../src/components/footer/footer';

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <link href="/static/css/style.css" rel="stylesheet"></link>
        </Head>
        <body className="custom_class">
          <Header />
          <Main />
          <Footer />
          <NextScript />
        </body>
      </html>
    )
  }
}