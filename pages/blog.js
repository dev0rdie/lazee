import React from 'react';
import ReactDom from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import {Intent, Spinner, Breadcrumbs, Breadcrumb, Icon, Colors, Card, Elevation, Button} from '@blueprintjs/core'
import SocialLogo from 'social-logos';
import GridiconBookMark from 'gridicons/dist/bookmark'
import GridiconPosts from 'gridicons/dist/posts';
import GridiconBook from 'gridicons/dist/book';
import GridiconChevron from 'gridicons/dist/chevron-right';
import Router from '../routes';
export default class Blog extends React.Component {

    static async getInitialProps(props) {
        this.props = props;

        return {asPath: this.props.asPath};
    }

    constructor(props){
        super(props);

        console.log(props);

        this.state = {
            breadcrumbs: [
                { href: "/", text: <React.Fragment><GridiconBook current fill={Colors.DARK_GRAY4} style={{verticalAlign: 'middle', marginTop: '-4px'}}/>blog</React.Fragment>},
                { href: "/section/", text: <React.Fragment><GridiconBookMark fill={Colors.DARK_GRAY4} style={{verticalAlign: 'middle', marginTop: '-4px'}}/>section</React.Fragment>},
                { href: "/article/", text: <React.Fragment><GridiconPosts fill={Colors.DARK_GRAY4} style={{verticalAlign: 'middle', marginTop: '-4px'}}/>title</React.Fragment>},
            ],
            path: props.asPath
        };

    }

    renderCurrentBreadCrumbs = () => {
        const breadCrumbs = this.state.breadcrumbs.map(({href, text}, index)=>{ return <Breadcrumb href={href} current={href === this.state.path}>{text} {index+1 < this.state.breadcrumbs.length ? <GridiconChevron style={{verticalAlign: 'middle', marginTop: '-4px'}}/> : null}</Breadcrumb>; });
        const breadcrumbsComponent = <React.Fragment><div class="breadcrumbs" style={{marginLeft: '15px'}}>{breadCrumbs}</div></React.Fragment>; 
        return(breadcrumbsComponent);
    };

    render() {
        return (
            <div style={{marginTop: '15px'}}>
                <React.Fragment>
                    {this.renderCurrentBreadCrumbs()}
                    <Spinner size={50}></Spinner>
                </React.Fragment>
                <div className="container-fluid">
                    <div className="row article-list">
                        <div className="col-lg-3">
                            <Card interactive={true} elevation={Elevation.TWO}>
                                <h5><a href="#">Card heading</a></h5>
                                <p>Card content</p>
                                <span class="bp3-tag bp3-round">react</span>
                            </Card>
                        </div>
                        <div className="col-lg-3">
                            <Card interactive={true} elevation={Elevation.TWO}>
                                <h5><a href="#">Card heading</a></h5>
                                <p>Card content</p>
                                <span class="bp3-tag bp3-round">react</span>
                            </Card>
                        </div>
                        <div className="col-lg-3">
                            <Card interactive={true} elevation={Elevation.TWO}>
                                <h5><a href="#">Card heading</a></h5>
                                <p>Card content</p>
                                <span class="bp3-tag bp3-round">react</span>
                            </Card>
                        </div>
                        <div className="col-lg-3">
                            <Card interactive={true} elevation={Elevation.TWO}>
                                <h5><a href="#">Card heading</a></h5>
                                <p>Card contentt</p>
                                <span class="bp3-tag bp3-round">react</span>
                            </Card>
                        </div>
                    </div>
                    
                </div>
            </div>
        );
    }

}